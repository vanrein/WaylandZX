# Wayland port of the ZX Spectrum emulator

> *The program `fbzx` is a lovely ZX Spectrum emulator, originally designed
> for framebuffer and later ported to SDL and X11.  Rather than using Wayland
> indirectly via SDL or XWayland, the executable `WaylandZX` directly works
> on Wayland.*

**This is work in progress.**
Emulation works on Sway, but not on Weston (without `wl_keyboard` service).
Rendering the screen is done in the Wayland screen as well as in the emulator, so we can later choose to be lazy.

The benefits of a separate program for Wayland are:

  * Improved performance, also thanks to timing/redrawing integration
  * No dependency on SDL and, as a result, no dependency on X11
  * Separate program == separate package == separate dependencies
  * New functionality (see below)

Note that SDL2 is still a compiler dependency; some SDL identifiers are
in the emulator (notably for key handling) so that code remains unchanged, and
a simply mapping from Wayland is then used.  The SDL2 library is not linked
in.  When compiling with `USE_SOUND_ALSA=1 make`, this yields a great reduction
in the number of libraries used (sampled as going down from 44 to 11).

The intention is not to fork the
[original FBZX simlator](https://gitlab.com/rastersoft/fbzx)
but rather to offer patches that can improve it.  The addtions below
may or may not be ported to the other platforms as well.

## Added functionality

The direct attachment to Wayland enables a few extra features:

  * **Screen locking** is a mode where the screen locks into the
    ZX Spectrum start screen.  The ZX Spectrum is fully functional.
    Press `Esc` to bring up a login dialog (dislayed with ZX Spectrum
    font and colour subtlety).  If your login fails, the ZX Spectrum
    simply continues.

  * **Login manager** runs a Wayland login procedure, which makes
    your computer boot into a ZX Spectrum.  The remainder works as
    for screen locking.

  * **Grace period** for screen locking may be a few seconds to
    allow restarting with `Esc` but no password entry.  This should
    be used when the screen locks after an inactivity timeout.

  * **Suspend and resume** at the moment your press `Esc` until you
    start again.  This requires a `.z80` storage file name argument;
    if it cannot be loaded, a fresh ZX Spectrum will be started, so
    a temporary file may be used.  When a suspend file is set, this
    mode exits the plain emulator after a single `Esc` instead of
    the normal precaution of pressing it twice to avoid state loss.

  * **Change root** is a necessity for secure screen locking; it
    causes the ZX Spectrum to have access to only a certain directory
    with tape files and such.  This requires the `CAP_SYS_CHROOT`
    capability to be set on the binary.

  * **Tape listing** is a constraining list of tapes and `.z80`
    files that may be used, and even MDR files with a microdrive.
    One of the tape files may be loaded randomly when resumption
    fails to load prior `.z80` state.  The microdrive can be used
    to load a file by name.

  * **Sleep mode** may be used to catch a ZX Spectrum caught in a
    ROM-based keyboard loop, and avoid its active polling until
    an interrupt has arrived.


## Wayland Keyboard

The basic `wl_keyboard` service is used to read keyboad events.
Repeated keypress-events are ignored, because the ZX Spectrum
does its own, driven by admirable `PEEK` and `POKE` controls.

There is no key entry buffer, but for one key event.  The readout
of keyboard events is done through independent polling, which is
certainly up for improvement.  Event-driven Z80 code, what an idea!


## Wayland Rendering

The screen updates for Wayland are as lazily as possible:

  * The first and last border line that changed; only changed lines
    will be rendered as "damaged" areas.

  * The characters that changed; only those that had pixels written
    in them, or their colour changed, will be rendered as "damaged"
    characters.  To support printing, horizontal sequences of
    characters are grouped in one "damage" claim; to support icons,
    the changes to a line are tested to apply on subsequent lines,
    to be grouped in one "damaged" area.

  * When no area is damaged, no buffer will be attached to the output
    surface.  This means that Wayland does not have to update the
    screen.

**Border.**
When the border colour changes, it is filled with the old colour
from the last point up to the one before the current time.  This is
also done when filling the remainder of the screen.  Flags record
if the border colour changed during the last screen period.  This
is used to avoid the redrawing in the next.

**Characters.**
There are 24 lines of 32 characters, each represented in a 32-bit
word of change flags.  Any pixel or colour damage in the area of
that character raises the flag, for rendering on the next screen.
The colour byte is stored, and indexes a colour index map for the
two colours used.  The character itself stores as 8x8 pixels, to
store in an `uint64_t [24][32]` array.  Characters are drawn by
selecting their 64 bits and updating the 64 offsets in the output
bitmap plane, filling each the appropriate colour from the index
for that character.

**Flashing.**
When the emulator decides to toggle the flash to or from reverse
video rendering, it reports that back to the Wayland screen.
The characters with a flash bit are then marked as dirty, so they
get redrawn.  While redrawing, their content is flipped when the
intention is reverse video, or kept as is otherwise.  This is the
closest possible to event-driven lazy flashing.
