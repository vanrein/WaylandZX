#ifndef SRC_MOUSE_HH_
#define SRC_MOUSE_HH_

#include "z80free/Z80free.h"
#include <inttypes.h>
#include <SDL2/SDL.h>

class Mouse {
	int posx;
	int posy;

public:
	uint8_t x;
	uint8_t y;
	uint8_t button;
	bool enabled;

	Mouse();
	void reset();
	void emulate(int);

	uint8_t in(uint16_t);
};

extern class Mouse *mouse;
#endif
