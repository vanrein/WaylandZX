/*
 * Copyright 2023-2023 (C) Rick van Rein
 * Copyright 2003-2015 (C) Raster Software Vigo (Sergio Costas)
 * This file is part of FBZX, but almost completely rewritten to make
 * it run as Wayland ZX, or WLZX.
 *
 * FBZX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FBZX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <unistd.h>
#include <limits.h>
#include <time.h>
#include <sys/mman.h>

#include <syscall.h>

#include "llscreen.hh"
#include "screen.hh"
#include "font.h"
#include "osd.hh"

#include <wayland-client.h>
#include "xdg-shell.h"


LLScreen *llscreen = NULL;

extern class Screen *screen;



extern "C" {


/*
 *
 * Wayland implementation of the LLScreen class.
 *
 */



/* Input interfaces/devices
 */
struct wl_display    *wlzx_display  = NULL;
struct wl_registry   *wlzx_registry = NULL;
struct wl_shm        *wlzx_shm      = NULL;
struct wl_seat       *wlzx_seat     = NULL;
struct wl_keyboard   *wlzx_keyboard = NULL;
struct wl_pointer    *wlzx_pointer  = NULL;
struct wl_compositor *wlzx_manager  = NULL;
struct xdg_wm_base   *wlzx_wm_base  = NULL;
struct wl_surface    *wlzx_surface  = NULL;
struct xdg_surface   *wlzx_tvscreen = NULL;
struct xdg_toplevel  *wlzx_mywindow = NULL;

/* File descriptor for the Wayland connection.
 */
int wlzx_display_fd = -1;

/* Screen pixel buffer, pool, buffer
 */
uint32_t *wlzx_pixels = NULL;
struct wl_shm_pool *wlzx_pool = NULL;
struct wl_buffer *wlzx_buffer = NULL;

/* The source for screen information.  The data may be drawn from
 * the wlzx_char_xxx below, or it may be drawn by the emulator,
 * in which case it may be stable or dirty as flagged herein.
 */
bool wlzx_screen_z80 = true;

/* Mark the entire screen as dirty.  This makes sense at the start
 * and when a menu has popped up or been removed.
 */
bool wlzx_screen_dirty = true;

/* A crude marker for dirty regions _somewhere_ in the border.
 * This may end up being simpler than redrawing precisely the bits
 * of the border that have changed.  It will draw the entire border
 * twice after any change, but not forever on any screen.  While
 * saving or loading, the changes will overlap, and each screen gets
 * drawn, which makes complete sense.  The dirty signal is a bitwise
 * or of 0x01 the next screen rendered and 0x02 the one following it.
 */
#define WLZX_BORDER_CLEAN        0x00
#define WLZX_BORDER_DIRTY_BOTTOM 0x01
#define WLZX_BORDER_DIRTY_TOP    0x02
#define WLZX_BORDER_DIRTY        0x03
//
uint8_t wlzx_border_dirty = WLZX_BORDER_CLEAN;

/* Parts of the screen that are dirty; 24 lines, 32 columns/bits each.
 * The word index is the y position of the character, where top  is 0.
 * The bit number is the x position of the character, where left is 0.
 */
uint32_t wlzx_char_dirty [24];

/* Colour codes with flash and bright attributes for each screen character.
 */
uint8_t wlzx_char_attrs [24][32];

/* Whether this screen is currently reversing flash characters.
 */
bool wlzx_flash_reverse_video;

/* The colour code 0..7 for the border, along with the timing details
 * that help to locate it, render it up to a point of change and, very
 * importantly, decide on the lines that were damaged by doing so.  If
 * no new border colour is set, then the next screen must still render
 * a fresh border up to and including the line that changed last.
 */
uint32_t wlzx_last_border_colour = 0xffffffff;
uint16_t wlzx_last_border_scrx = 0;
uint16_t wlzx_last_border_scry = 0;
uint16_t wlzx_border_damage_from = 0;
uint16_t wlzx_border_damage_upto = 255;
uint16_t wlzx_border_damage_upto_next = 0;

/* Character content in 64 bits each, at a character position at a time.
 * The _stored_ byte order is as in the character map in the ZX Spectrum ROM.
 * So, on little endian machines, the low byte holds the top 8 pixels, and
 * the high byte holds the low 8 pixels.
 */
union wlzx_pixel_t {
	uint64_t bitmap64;
	uint8_t bitmap8 [8];
};
wlzx_pixel_t wlzx_char_bitmaps [24][32];


/* Demand a non-NULL pointer, exit otherwise.
 */
void *wlzx_demand (const char *name, void *ptr) {
	if (ptr == NULL) {
		fprintf (stderr, "Wayland fatal error: no %s\n", name);
		exit (1);
	}
	return ptr;
}



/* Registry event handlers and listener.
 */
void wlzx_registry_add (void *data, wl_registry *self,
			uint32_t name, const char *interface, uint32_t version) {
	/*..*/ if (0 == strcmp (interface, wl_shm_interface.name)) {
		wlzx_shm = (struct wl_shm *) wl_registry_bind (
				wlzx_registry, name, &wl_shm_interface, 1);
	} else if (0 == strcmp (interface, wl_seat_interface.name)) {
		wlzx_seat = (struct wl_seat *) wl_registry_bind (
				wlzx_registry, name, &wl_seat_interface, 7);
	} else if (0 == strcmp (interface, wl_keyboard_interface.name)) {
		wlzx_keyboard = (struct wl_keyboard *) wl_registry_bind (
				wlzx_registry, name, &wl_keyboard_interface, 1);
	} else if (0 == strcmp (interface, wl_compositor_interface.name)) {
		wlzx_manager = (struct wl_compositor *) wl_registry_bind (
				wlzx_registry, name, &wl_compositor_interface, 4);
	} else if (0 == strcmp (interface, xdg_wm_base_interface.name)) {
		wlzx_wm_base = (struct xdg_wm_base *) wl_registry_bind (
				wlzx_registry, name, &xdg_wm_base_interface, 2);
	} else { };
}
//
void wlzx_registry_del (void *data, wl_registry *self,
			uint32_t name) {
	;
}
//
struct wl_registry_listener wlzx_registry_listener = {
	.global        = wlzx_registry_add,
	.global_remove = wlzx_registry_del,
};


/* Keyboard event handlers and listener.
 */
void wlzx_keyboard_key (void *data, struct wl_keyboard *self,
			uint32_t serial, uint32_t time, uint32_t key, uint32_t state);
//
void wlzx_keyboard_modifiers (void *data, struct wl_keyboard *self,
			uint32_t serial,
			uint32_t mods_depressed, uint32_t mods_latched, uint32_t mods_locked,
			uint32_t group);
//
void wlzx_keyboard_keymap (void *data, struct wl_keyboard *self,
			uint32_t format, int32_t fd, uint32_t size) {
	;
}
//
void wlzx_keyboard_enter (void *data, struct wl_keyboard *self,
			uint32_t serial, struct wl_surface *surface, struct wl_array *keys);
//
void wlzx_keyboard_leave (void *data, struct wl_keyboard *self,
			uint32_t serial, struct wl_surface *surface);
//
void wlzx_keyboard_repeat_info (void *data, struct wl_keyboard *self,
			int32_t rate, int32_t delay) {
	;
}
//
struct wl_keyboard_listener wlzx_keyboard_listener = {
	.keymap      = wlzx_keyboard_keymap,
	.enter       = wlzx_keyboard_enter,
	.leave       = wlzx_keyboard_leave,
	.key         = wlzx_keyboard_key,
	.modifiers   = wlzx_keyboard_modifiers,
	.repeat_info = wlzx_keyboard_repeat_info,
};


/* Surface handlers and listener.
 */
void wlzx_tvscreen_configure (void *data, struct xdg_surface *self,
			uint32_t serial) {
	xdg_surface_ack_configure (self, serial);
}
//
struct xdg_surface_listener wlzx_tvscreen_listener = {
	.configure = wlzx_tvscreen_configure,
};


/* Initialise Wayland
 */
void wlzx_init (int16_t width, int16_t height) {
	//
	// Calculate sizes
	int stride = width * sizeof (uint32_t);
	int size = stride * height;
	//
	// Find initial global objects
	wlzx_display  = (struct wl_display  *) wlzx_demand ("display",
				wl_display_connect (NULL));
	wlzx_registry = (struct wl_registry *) wlzx_demand ("registry",
				wl_display_get_registry (wlzx_display));
	wlzx_display_fd = wl_display_get_fd (wlzx_display);
	//
	// Iterate the registry, in search of desired objects
	wl_registry_add_listener (wlzx_registry, &wlzx_registry_listener, NULL);
	wl_display_roundtrip (wlzx_display);
	wlzx_demand ("wl_seat", wlzx_seat);
	wlzx_demand ("xdg_wm_base", wlzx_wm_base);
	//
	// Configure keyboard handlers
	wlzx_keyboard = (struct wl_keyboard *) wlzx_demand ("wl_keyboard",
			wl_seat_get_keyboard (wlzx_seat));
	wl_keyboard_add_listener (wlzx_keyboard, &wlzx_keyboard_listener, NULL);
	//
	// Configure a desktop window (known in Wayland as a "toplevel surface")
	wlzx_surface = (wl_surface *) wlzx_demand ("wlzx_surface",
				wl_compositor_create_surface (wlzx_manager));
	wlzx_tvscreen = (xdg_surface *) wlzx_demand ("wlzx_tvscreen",
				xdg_wm_base_get_xdg_surface (wlzx_wm_base, wlzx_surface));
	xdg_surface_add_listener (wlzx_tvscreen, &wlzx_tvscreen_listener, NULL);
	wlzx_mywindow = (xdg_toplevel *) wlzx_demand ("wlzx_mywindow",
				xdg_surface_get_toplevel (wlzx_tvscreen));
	//TODO// xdg_toplevel_add_listener (wlzx_mywidow, &wlzx_mywindow_listener, NULL);
	wl_surface_commit (wlzx_surface);
	//
	// Now construct a shared memory pool and buffer
	int fd = syscall (SYS_memfd_create, "buffer", 0);
	ftruncate (fd, size);
	wlzx_pixels = (uint32_t *) wlzx_demand ("wlzx_pixels",
			mmap (NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0));
	wlzx_pool = (struct wl_shm_pool *) wlzx_demand ("wlzx_pool",
			wl_shm_create_pool (wlzx_shm, fd, size));
	wlzx_buffer = (struct wl_buffer *) wlzx_demand ("wlzx_buffer",
			wl_shm_pool_create_buffer (wlzx_pool, 0, width, height, stride, WL_SHM_FORMAT_XRGB8888));
	wl_display_roundtrip (wlzx_display);
	//
	// Show the display for the first time
	wl_surface_attach (wlzx_surface, wlzx_buffer, 0, 0);
	wl_surface_commit (wlzx_surface);
	wl_display_roundtrip (wlzx_display);
}


/* Finalise Wayland.
 *
 * This function is conditional on things having been created;
 * it takes notes of things cleaned up to make this idempotent.
 */
void wlzx_fini (int16_t width, int16_t height) {
	//
	// Calculate sizes
	int stride = width * sizeof (uint32_t);
	int size = stride * height;
	//
	// Cleanup the seat
	if (wlzx_keyboard) {
		wl_keyboard_release (wlzx_keyboard);
		wlzx_keyboard = NULL;
	}
	if (wlzx_seat) {
		wl_seat_release (wlzx_seat);
		wlzx_seat = NULL;
	}
	//
	// Cleanup system resources
	if (wlzx_pixels) {
		munmap (wlzx_pixels, size);
		wlzx_pixels = NULL;
	}
}


/* Wayland possibly-blocking dispatcher for any events due to arrive and/or queued.
 */
void wlzx_dispatch (bool blockwait) {
	//
	// Fixed timeout
	static struct timeval nowaiting = {
		.tv_sec  = 0,
		.tv_usec = 0,
	};
	//
	// Be sure to have a display to listen to
	if (wlzx_display_fd < 0) {
		fprintf (stderr, "Wayland: No display fd, ignoring events\n");
		return;
	}
	//
	// Possibly decide for the blockwait variant if data is pending
	if (!blockwait) {
		fd_set poller;
		FD_ZERO (&poller);
		FD_SET (wlzx_display_fd, &poller);
		blockwait = (select (wlzx_display_fd + 1, &poller, NULL, &poller, &nowaiting) >= 1);
	}
	//
	// Determine how to proceed, based on data to arrive or not
	if (blockwait) {
		//
		// Data is waiting on the Wayland server socket or
		// the caller is asking us to block-wait for events
		wl_display_dispatch (wlzx_display);
	} else {
		//
		// No data waiting on the Wayland server socket
		wl_display_dispatch_pending (wlzx_display);
	}
}


/* Wayland handler loop
 */
void wlzx_loop (void) {
	printf ("Wayland enters infinite loop\n");
	while (true) {
		wlzx_dispatch (true);
	}
}


/* Fill the border with the last colour, starting at the last point and going
 * up to (scrx,scry) as given here.  The latter point is not included, it will
 * be the first to receive the new colour.
 *
 * Note how this work is always done too late; this is done to avoid drawing
 * ahead of time and later needing to override it, while having damaged too
 * much of the buffer.
 */
void wlzx_border_fill (uint16_t scrx, uint16_t scry) {
	//
	// Find the counter values and the first pixel to change
	uint16_t ctrx = wlzx_last_border_scrx;
	uint16_t ctry = wlzx_last_border_scry;
	uint32_t *pixels = wlzx_pixels + ctry * 640 + ctrx;
	//
	// Loop until (ctrx,ctry) match (scrx,scry)
	while (ctry <= scry) {
		//
		// Find how far to go on this line
		uint16_t maxx = (ctry == scry) ? scrx : 640;
		//
		// Determine whether there is a character gap to mind
		bool content_gap = (ctry < 48 + 2 * 192) && (ctry >= 48);
		uint16_t maxx0 = content_gap ? 64 : maxx;
		if (maxx0 > maxx) {
			maxx0 = maxx;
		}
		//
		// Loop over the pre-gap portion (possibly the whole line)
		while (ctrx < maxx0) {
			*pixels++ = wlzx_last_border_colour;
			ctrx++;
		}
		//
		// Skip the gap, if any, and possibly render more
		if (content_gap) {
			uint16_t resync = 64 + 2 * 256 - ctrx;
			pixels += resync;
			ctrx   += resync;
			while (ctrx < maxx) {
				*pixels++ = wlzx_last_border_colour;
				ctrx++;
			}
		}
		//
		// Move coordinates to the beginning of the next line
		ctry++;
		ctrx = 0;
	}
	//
	// Mark the next two screen renderings as dirty
	wlzx_border_dirty = WLZX_BORDER_DIRTY;
}


/* Finish the border to the end of the page, if it is not there yet.
 */
void wlzx_border_finish_page (void) {
	if ((wlzx_last_border_scrx != 640) || (wlzx_last_border_scry != 479)) {
		//
		// Fill the remainder of the border
fprintf (stderr, "wlzx_border_fill/page at %p, (%d,%d) in #%06x --> (%d,%d)\n", wlzx_pixels, wlzx_last_border_scrx, wlzx_last_border_scry, wlzx_last_border_colour, 640, 479);
		wlzx_border_fill (640, 479);
		//
		// Avoid further writes
		wlzx_last_border_scrx = 640;
		wlzx_last_border_scry = 479;
	}
}


}   /* End of extern "C" */



/* Create and initialise a new Wayland LLScreen object.
 */
LLScreen::LLScreen(int16_t width, int16_t height, uint8_t _depth, bool fullscreen, bool dblbuffer, bool hwsurface, bool setres) {
	//
	// Recycle the WaylandZX globals (_fini is conditional and idempotent)
	wlzx_fini (width, height);
	wlzx_init (width, height);
	wlzx_dispatch (false);
	//
	// Setup the geometry
	this->width  = width;
	this->height = height;
	this->memory = (uint32_t *) wlzx_pixels;
	//
	// Setup other parameters
	this->setres  = setres;
	this->rotate  = false;
	this->cheight = (charset.max_top > charset.height_maxtop)
				? charset.max_top
				: charset.height_maxtop;
	this->lines_in_screen = 480 / this->cheight;
	//
	// Do not assume rotation
	this->rotate = false;
	//
	// If this needs to go fullscreen, make it happen
	if (fullscreen) {
		this->fullscreen_switch ();
	}
}


LLScreen::~LLScreen () {
	//
	// Cleanup the WaylandZX globals
	wlzx_fini (this->width, this->height);
}

/* Update mouse grabbing; only grab for fullscreen mode.
 */
void LLScreen::set_mouse () {
	if (this->fullscreen) {
		fprintf (stderr, "TODO: Grab the mouse\n");
	} else {
		fprintf (stderr, "TODO: Ungrab the mouse\n");
	}
}

/* Toggle fullscreen mode
 *
 * TODO: Overlay? Grab? Lock?
 */
void LLScreen::fullscreen_switch () {
	//
	// Switch from or to fullscreen mode
	if (this->fullscreen) {
		xdg_toplevel_unset_fullscreen (wlzx_mywindow);
		this->fullscreen = false;
	} else {
		xdg_toplevel_set_fullscreen (wlzx_mywindow, NULL);
		this->fullscreen = true;
	}
	//
	// Grab the mouse for fullscreen mode only
	set_mouse ();
}


/* Form the screen and ship it out to the screen.
 * Variants exist for the Z80 screen (wlzx_screen_z80).  The whole screen
 * may be marked dirty (wlzx_screen_dirty) or characters (wlzx_char_dirty[]).
 */
void LLScreen::do_flip () {
	//
	// Attach the buffer (again) to the surface
	wl_surface_attach (wlzx_surface, wlzx_buffer, 0, 0);
	//
	// The fbzx model is to always render (emulating the hardware)
	// but we now receive the character data via LLScreen::poke()
	// and use the address to mark parts of the screen as dirty
	// so we do not need to make the whole screen dirty anymore
	// wlzx_screen_dirty = true;
	//
	// Update the screen if it is a Z80 screen with only Z80-driven dirt
	if (wlzx_screen_z80 && !wlzx_screen_dirty) {
		//
		// Iterate over lines, drawing characters that changed
		uint32_t *disppos = this->memory + 48 * this->width + 64;
		for (uint8_t line = 0; line < 24; line++) {
			//
			// Mark this line as no longer dirty, then handle it
			uint32_t dirt = wlzx_char_dirty [line];
			wlzx_char_dirty [line] = 0;
			//
			// Iterate until all dirty characters are found
			uint8_t col = 0;
			while (dirt != 0) {
				//
				// Skip to the next dirty column
				while ((dirt & 0x00000001) == 0x00000000) {
					dirt >>= 1;
					col++;
				}
				//
				// Count consecutive dirty columns
				uint8_t colseq = 0;
				while ((dirt & 0x00000001) == 0x00000001) {
					dirt >>= 1;
					colseq++;
				}
				//
				// Draw the dirty column sequence
				for (uint8_t seqi = col; seqi < col + colseq; seqi++) {
					//
					// Collect ink and paper colours for the character
					uint32_t col_ink   = this->colors [(wlzx_char_attrs [line][seqi]     ) & 0x07];
					uint32_t col_paper = this->colors [(wlzx_char_attrs [line][seqi] >> 3) & 0x07];
					/* TODO: ULAplus, BRIGHT, FLASH */
					//
					// Fetch the character bitmap
					uint64_t bitmap = wlzx_char_bitmaps [line][seqi].bitmap64;
					//
					// Possibly reverse video because of flash
					if (wlzx_flash_reverse_video && (wlzx_char_attrs [line][seqi] & 0x80)) {
						bitmap = ~bitmap;
					}
					//
					// Iterate over pixel positions, while using scaling factor 2
					uint32_t *pixpos = disppos + 16 * seqi;
					for (uint8_t pixv = 0; pixv < 16; pixv += 2) {
						for (uint8_t pixh = 14; pixh < 16; pixh -= 2) {
							pixpos [pixh + 1 + this->width] =
							pixpos [pixh + 0 + this->width] =
							pixpos [pixh + 1] =
							pixpos [pixh + 0] = (bitmap & 0x00000001) ? col_ink : col_paper;
							bitmap >>= 1;
						}
						pixpos += 2 * this->width;
					}
				}
				//
				// Mark the dirty column sequence as damaged
				//LOGICALLY// wl_surface_damage_buffer (wlzx_surface, 8 * col, 8 * line, 8 * colseq, 8);
				wl_surface_damage_buffer (wlzx_surface, 64 + 16 * col, 48 + 16 * line, 16 * colseq, 16);
				//
				// Skip over the now-handled columns
				col += colseq;
			}
			//
			// Move on to the pixels of the next character line
			disppos += this->width * 16;
		}
	}
	//
	// Damage the border, in 4 separate regions
	//TODO// Constrain to what really changed
	if (wlzx_border_dirty != WLZX_BORDER_CLEAN) {
		wlzx_border_finish_page ();
		wlzx_last_border_scrx = 0;
		wlzx_last_border_scry = 0;
fprintf (stderr, "Damaging the border area\n");
		wl_surface_damage_buffer (wlzx_surface, 0, 0, 640, 48);
		wl_surface_damage_buffer (wlzx_surface, 0, 48, 64, 2 * 192);
		wl_surface_damage_buffer (wlzx_surface, 64 + 2 * 256, 48, 64, 2 * 192);
		wl_surface_damage_buffer (wlzx_surface, 0, 48 + 2 * 192, 640, 480 - 48 - 2 * 192);
		wlzx_border_dirty >>= 1;
	}
	//
	// If the entire screen is written and dirty, mark it entirely damaged
	if (wlzx_screen_dirty) {
		wl_surface_damage_buffer (wlzx_surface, 0, 0, INT32_MAX, INT32_MAX);
		wlzx_screen_dirty = false;
	}
	//
	// We finished our updates, so commit them
	//TODO// Receive notify when a new screen is needed
	wl_surface_commit (wlzx_surface);
}


/* Erase the screen.  This is used for emulator control, so it
 * can work directly on the buffer.
 */
void LLScreen::clear_screen () {
	memset (wlzx_pixels, 0, this->width * this->height * sizeof (uint32_t));
	//TODO//NO_RETURN_YET// wlzx_screen_z80   = false;
	wlzx_screen_dirty = true;
}


/* Paint the one pixel at the given address in the colour index.
 */
void LLScreen::paint_one_pixel (uint8_t colour_index, uint32_t *pixel_address) {
	//
	// Set the pixel to the colour as taken from the colour index
	*pixel_address = this->colors [colour_index];
}


/* Paint the picture in the named file on the screen.  It shows the
 * keyboard layout.
 */
void LLScreen::paint_picture (string filename) {
	//
	// Open the picture filename
	ifstream *pixfile = myfopen (filename, ios::in | ios::binary);
	if (pixfile == NULL) {
		osd->set_message ("Picture file not found", 2000);
		return;
	}
	//
	// Clear the screen before starting to paint
	this->clear_screen ();
	//
	// Copy the picture into the buffer memory
	uint32_t *buffer = this->memory;
	if (!this->rotate) {
		//
		// Copy the picture without rotation
		for (int pixctr = 0; pixctr < 344 * 640; pixctr++) {
			uint8_t colidx;
			pixfile->read ((char *) &colidx, 1);
			paint_one_pixel (colidx, buffer);
			buffer++;
		}
	} else {
		buffer += 479;
		for (int pixrow = 0; pixrow < 344; pixrow++) {
			uint32_t *buffer2 = buffer;
			for (int pixcol = 0; pixcol < 640; pixcol++) {
				uint8_t colidx;
				pixfile->read ((char *) &colidx, 1);
				paint_one_pixel (colidx, buffer);
				buffer += 480;
			}
			buffer = buffer2 - 1;
		}
	}
	//
	// Cleanup the picture file resources
	pixfile->close();
	delete pixfile;
	//
	// Update the dirty region administration
	//TODO//NO_RETURN_YET// wlzx_screen_z80   = false;
	wlzx_screen_dirty = true;
}


/* Make a screenshot to a file with a standard-pattern name,
 */
void LLScreen::do_screenshot (void) {
	char filename[256];
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);
	sprintf (filename, "fbzx_screenshot_%d_%02d_%02d_%02d_%02d_%02d.bmp",
		tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
	fprintf (stderr, "TODO: Write a bitmap to \"%s\"\n", filename);
	//osd->set_message ("Done screenshot", 2000);
	osd->set_message ("TODO: Screenshotting code", 2000);
}


/* Open a file in a given mode.  Try various locations.
 */
ifstream *LLScreen::myfopen (string filename, ios_base::openmode mode) {
	char      tmp [PATH_MAX + 5];
	ifstream *file;
	//
	// Try to open the file at the given location
	file = new ifstream (filename.c_str(), mode);
	if (file->is_open ()) {
		return file;
	}
	//
	// Another try, treating the file as relative to /usr/share
	delete(file);
	snprintf (tmp, PATH_MAX, "/usr/share/%s", filename.c_str());
	file = new ifstream (tmp, mode);
	if (file->is_open ()) {
		return file;
	}
	//
	// Another try, treating the file as relative to /usr/local/share
	delete (file);
	snprintf (tmp, PATH_MAX, "/usr/local/share/%s", filename.c_str());
	file = new ifstream (tmp, mode);
	if (file->is_open ()) {
		return file;
	}
	//
	// All our attempts failed
	delete (file);
	return NULL;
}


/* Take note of a write into video memory.  Find its (x,y) relative to
 * top-left (0,0) and detect if this is an attribute.  Then update the
 * Wayland screen accordingly, marking the necessary areas as damaged.
 */
void LLScreen::poke (uint16_t Addr, uint8_t Value) {
	//
	// Determine the (x,y) coordinates written, top-left is (0,0)
	// Find an attr flag to signal the writing of an attribute
	uint8_t x = Addr & 0x1f;
	uint8_t y = (Addr - 16384) >> 5;
	bool attr = (y >= 192);
	if (attr) {
		y -= 192;
	} else {
		y = (y & 0x07) | ((y >> 3) & 0xf8);
	}
	//
	// Process any attributes being set...
	if (attr) {
		//
		// Check if anything changed, return if not
		if (Value == wlzx_char_attrs [y][x]) {
			return;
		}
		//
		// Poke the screen attributes
		wlzx_char_attrs [y][x] = Value;
	}
	//
	// ...or a byte in the screen character
	else {
		//
		// Decode the depth in the screen character, that is
		// the byte number counting from 0 top to 7 bottom
		uint8_t z = (Addr >> 8) & 0x07;
		//
		// During scrolls and editing, it may save resources
		// when equal characters are not considered dirty
		if (wlzx_char_bitmaps [y][x].bitmap8 [z] == Value) {
			return;
		}
		//
		// Shift and write out the masked value into the
		// 64-bit integer that is to hold the bitmap data
		wlzx_char_bitmaps [y][x].bitmap8 [z] = Value;
	}
	//
	// Mark (x,y) in video memory as dirty, to guide lazy rendering
	wlzx_char_dirty [y] |= ((uint32_t) 0x00000001) << x;
}


/* Take note of the output of a new (changed) border value to the output.
 * Combined with timing, this indicates where on the screen the change
 * occurs.  Draw the border pixels from the previous point in the previous
 * colour, and mark at least the border lines from the current point as
 * damaged, and in need of a redraw.
 *
 * Note that the next screen will also need a redraw up to and including
 * the current border position, and that this should also be set as damage.
 */
void LLScreen::out_border (uint8_t Value) {
	//
	// Take note of the new border colour (in a 0..7 range) starting at
	// the screen position being rendered at this wallclock time;
	// draw up from last time, and mark the screen region as dirty
	;
	//
	// Use pixel timing to derive the screen position for this change
	uint16_t scrx = 9999;
	uint16_t scry = 9999;
	screen->get_screen_position (&scrx, &scry);
	//
	// Scale up by a factor 2
	scrx *= 2;
	scry *= 2;
	//
	// Restrict values to display boundaries
	if (scry < 0) {
		scry = 0;
	}
	if (scrx < 0) {
		scrx = 0;
	}
	if (scrx > this->width) {
		scry++;
		scrx = 0;
	}
	if (scry > this->height) {
		scrx = this->width;
		scry = this->height - 1;
	}
	//
	// Reset the page if need be
	if ((scry < wlzx_last_border_scry) ||
			((scry == wlzx_last_border_scry) && (scrx < wlzx_last_border_scrx))) {
		wlzx_border_finish_page ();
		wlzx_last_border_scrx = 0;
		wlzx_last_border_scry = 0;
	}
	//
	// Fill the last colour up to the current change point
	wlzx_last_border_colour = this->colors [Value];
fprintf (stderr, "wlzx_border_fill/line at %p, (%d,%d) in #%06x --> (%d,%d)\n", wlzx_pixels, wlzx_last_border_scrx, wlzx_last_border_scry, wlzx_last_border_colour, scrx, scry);
	wlzx_border_fill (scrx, scry);
	//
	// Ensure proper dirty settings; this line is always included
	if (wlzx_border_damage_from > scry) {
		wlzx_border_damage_from = scry;
	}
	if (wlzx_border_damage_upto_next < scry) {
		wlzx_border_damage_upto_next = scry;
	}
	//
	// Store the new border colour and timing to be the last values
	wlzx_last_border_scrx = scrx;
	wlzx_last_border_scry = scry;
}


/* Take note of a change in sign for reverse video or not.  This is used
 * to handle flashing characters.  Assume that the new flag differs from
 * the previous.
 *
 * Two implementation strategies are now possible.  One is to mark as dirty
 * any character that has its flash flag set, which causes rendering of
 * the entire character.  The other is reversing the video, and marking
 * those as damaged from this routine, as a speed way variety.
 */
void LLScreen::toggle_flash (bool reverse) {
	//
	// Store the flag for our next rendering party
	wlzx_flash_reverse_video = reverse;
	//
	// Mark characters with the flash bit set as dirty
	for (uint8_t y = 0; y < 24; y++) {
		for (uint8_t x = 0; x < 32 ; x++) {
			if (wlzx_char_attrs [y][x] & 0x80) {
				wlzx_char_dirty [y] |= 0x00000001 << x;
			}
		}
	}
}



			//////TODO/////OLD/////CODE/////



void LLScreen::set_palete_entry(uint8_t entry, uint8_t V, bool bw) {
	uint32_t r, g, b, Value, max;
	
	Value = (uint32_t) V;
	
	if (entry >= 16) {
		this->ulaplus_palete[entry - 16] = Value;
		r = ((Value << 3) & 0xE0) + ((Value) & 0x1C) + ((Value >> 3) & 0x03);
		g = (Value & 0xE0) + ((Value >> 3) & 0x1C) + ((Value >> 6) & 0x03);
		b = ((Value << 6) & 0xC0) + ((Value << 4) & 0x30) + ((Value << 2) & 0x0C) + ((Value) & 0x03);
	} else {
		max = (V & 0x08) ? 0x000000FF : 0x000000C0;
		r   = (V & 0x02) ? max : 0;
		g   = (V & 0x04) ? max : 0;
		b   = (V & 0x01) ? max : 0;
	}
	
	if (bw) {
		uint32_t final;
		final = ((r * 30) + (g * 59) + (b * 11)) / 100;
		r = g = b = final;
	}
	// Color mode
	
	r <<= 16;
	r  &= 0x00FF0000;
	g <<= 8;
	g  &= 0x0000FF00;
	b  &= 0x000000FF;
	
	colors[entry] = 0xFF000000 | r | g | b;
}

uint8_t LLScreen::get_palete_entry(uint8_t entry) {
	return (this->ulaplus_palete[entry]);
}

void LLScreen::set_paletes(bool bw) {
	unsigned int c;
	
	for (c = 0; c < 80; c++) {
		if (c < 16) {
			this->set_palete_entry(c, c, bw);
		} else {
			set_palete_entry(c, this->ulaplus_palete[c - 16], bw);
		}
	}
}

// prints the ASCII character CHARAC in the framebuffer MEMO, at X,Y with ink color COLOR and paper color BACK, asuming that the screen width is WIDTH
uint8_t LLScreen::printchar(uint8_t carac, int16_t x, int16_t y, uint8_t color, uint8_t back) {
	int       bucle1, bucle2, offset;
	uint32_t *lugar, *lugar2;
	uint8_t   width;
	uint8_t   width2;
	uint8_t   height;
	int8_t    top;
	uint8_t * counter;
	
	carac -= 32;
	
	offset  = charset.offsets[carac];
	counter = charset.data + offset;
	width   = *(counter++);
	height  = *(counter++);
	counter++;
	top      = *counter;
	counter += 3;
	
	// y+=charset.max_top;
	lugar = this->memory + y * this->width + x;
	
	if (width < MIN_WIDTH) {
		width2 = MIN_WIDTH;
	} else {
		width2 = width + 1;
	}
	
	for (bucle1 = 0; bucle1 < (int) this->cheight; bucle1++) {
		lugar2 = lugar;
		if (bucle1 + y >= 480) {
			break;
		}
		for (bucle2 = 0; bucle2 < width2; bucle2++) {
			if ((bucle2 < width) && (bucle1 >= (charset.max_top - top - charset.min_top - 1) && (bucle1 < (charset.max_top - top + height - charset.min_top - 1)) && ((*(counter++)) > 127))) {
				paint_one_pixel(color, lugar2);
			} else {
				paint_one_pixel(back, lugar2);
			}
			lugar2++;
		}
		lugar += this->width;
	}
	return width2;
}

// prints the string CADENA in X,Y (centered if X=-1), with colors COLOR and BACK
void LLScreen::print_string(string o_cadena, int16_t x, float y, uint8_t ink, uint8_t paper) {
	unsigned int   length;
	int            ncarac, bucle, xx;
	uint16_t       xxx, yyy;
	unsigned char *cadena = (unsigned char *) o_cadena.c_str();
	unsigned char *str2;
	
	length = 0;
	ncarac = 0;
	for (str2 = cadena; *str2; str2++) {
		uint8_t  c;
		uint8_t  l;
		uint32_t offset;
		
		c = *str2;
		if (c >= ' ') {
			ncarac++;
			offset = charset.offsets[c - 32];
			l      = *(charset.data + offset);
			if (l < MIN_WIDTH) {
				l = MIN_WIDTH;
			} else {
				l++;
			}
			length += l;
		}
	}
	
	if (length > this->width) {
		if (x >= 0) {
			xx = x;
		} else {
			xx = 0;
		}
	} else {
		if (x == -1) { // we want it centered
			xx = (this->width / 2) - (length / 2);
		} else {
			xx = x;
		}
	}
	
	xxx = xx;
	if (y < 0) {
		yyy = 480 + y * this->cheight;
	} else {
		yyy = y * this->cheight;
	}
	str2 = cadena;
	for (bucle = 0; bucle < ncarac; bucle++) {
		uint8_t increment;
		while ((*str2) < ' ') {
			if ((*str2) == 1) {
				ink   = *(str2 + 1);
				str2 += 2;
				continue;
			}
			if (*str2 == 2) {
				paper = *(str2 + 1);
				str2 += 2;
				continue;
			}
			printf("Error de cadena %d %s\n", *str2, cadena);
			str2++;
		}
		increment = this->printchar(*str2, xxx, yyy, ink, paper);
		xxx      += increment;
		if (xxx >= width - charset.max_w) {
			xxx  = 0;
			yyy += this->cheight;
		}
		str2++;
	}
}
