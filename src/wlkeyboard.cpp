/*
 * Copyright 2023-2023 (C) Rick van Rein
 * This file is part of FBZX, only used in WaylandZX variant, or WLZX.
 *
 * FBZX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * FBZX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * WLZX keyboard handling.  Tap into Wayland input and deliver like SDL.
 * As a result, no SDL is required (and so, no X11 either).
 *
 */


#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include <unistd.h>

#include <wayland-client.h>

#include <SDL2/SDL.h>



extern "C" {


/* Wayland possibly-blocking dispatcher for any events due to arrive and/or queued.
 */
void wlzx_dispatch (bool blocking);


/* The keymap scancodes from a PC keyboard, directly passed to the
 * ZX Spectrum emulator in the form of SDLK_xxx codes.
 */
int32_t wlzx_keymap [] = {
	// 0 == unused
	SDLK_UNKNOWN,
	// First row: 1..14 ==  Esc, 1-0, -, +, BS
	SDLK_ESCAPE, SDLK_1, SDLK_2, SDLK_3, SDLK_4, SDLK_5, SDLK_6, SDLK_7, SDLK_8, SDLK_9, SDLK_0, SDLK_MINUS, SDLK_PLUS, SDLK_BACKSPACE,
	// Second row: 15..28 == TAB, Q-P, [, ], ENTER
	SDLK_TAB, SDLK_q, SDLK_w, SDLK_e, SDLK_r, SDLK_t, SDLK_y, SDLK_u, SDLK_i, SDLK_o, SDLK_p, SDLK_LEFTBRACKET, SDLK_RIGHTBRACKET, SDLK_RETURN,
	// Third row: 29..43 = LCTRL, a, s, d, f, g, h, j, k, l, ;, ', `, ?, LSHIFT, '\'
	SDLK_LCTRL, SDLK_a, SDLK_s, SDLK_d, SDLK_f, SDLK_g, SDLK_h, SDLK_j, SDLK_k, SDLK_l, SDLK_SEMICOLON, SDLK_QUOTEDBL, SDLK_UNKNOWN, SDLK_LSHIFT, SDLK_BACKSLASH,
	// Fourth row: 44..56 == z..m, COMMA, DOT, /, RSHIFT, ?, LALT
	SDLK_z, SDLK_x, SDLK_c, SDLK_v, SDLK_b, SDLK_n, SDLK_m, SDLK_COMMA, SDLK_PERIOD, SDLK_SLASH, SDLK_RSHIFT, SDLK_UNKNOWN, SDLK_LALT,
	// Fifth row: 57..58 == SPACE, CAPSLOCK
	SDLK_SPACE, SDLK_CAPSLOCK,
	// Sixth row: 59..68 == F1..F10
	SDLK_F1, SDLK_F2, SDLK_F3, SDLK_F4, SDLK_F5, SDLK_F6, SDLK_F7, SDLK_F8, SDLK_F9, SDLK_F10, SDLK_F11, SDLK_F12,
	// No keys: 69..85: NumLock, ?, Num7, 8, 9, Num-, 4, 5, 6, +, 1, 2, 3, 0, Num., 
	SDLK_NUMLOCKCLEAR, SDLK_UNKNOWN, SDLK_KP_7, SDLK_KP_8, SDLK_KP_9, SDLK_KP_MINUS, SDLK_KP_4, SDLK_KP_5, SDLK_KP_6, SDLK_KP_PLUS, SDLK_KP_1, SDLK_KP_2, SDLK_KP_3, SDLK_KP_0, SDLK_KP_PERIOD, SDLK_UNKNOWN, SDLK_UNKNOWN,
	// Misc: 86..88 == <>, F11, F12
	SDLK_LESS, SDLK_F11, SDLK_F12,
	// No keys: 89..96
	SDLK_UNKNOWN, SDLK_UNKNOWN, SDLK_UNKNOWN, SDLK_UNKNOWN, SDLK_UNKNOWN, SDLK_UNKNOWN, SDLK_UNKNOWN, SDLK_UNKNOWN,
	// Misc: 97..102 == RCTRL, ?. SysReq, RALT, ?, ?
	SDLK_RCTRL, SDLK_UNKNOWN, SDLK_SYSREQ, SDLK_RALT, SDLK_UNKNOWN, SDLK_UNKNOWN,
	// Misc: 103..111 == UP, PGUP, LEFT, RIGHT, ?, DOWN, PGDN, INS, DEL
	SDLK_UP, SDLK_PAGEUP, SDLK_LEFT, SDLK_RIGHT, SDLK_UNKNOWN, SDLK_DOWN, SDLK_PAGEDOWN, SDLK_INSERT, SDLK_DELETE,
	// No keys: 112..118
	SDLK_UNKNOWN, SDLK_UNKNOWN, SDLK_UNKNOWN, SDLK_UNKNOWN, SDLK_UNKNOWN, SDLK_UNKNOWN, SDLK_UNKNOWN,
	// Misc: 119 == Pause
	// no keys: 120..124
	SDLK_UNKNOWN, SDLK_UNKNOWN, SDLK_UNKNOWN, SDLK_UNKNOWN, SDLK_UNKNOWN,
	// Misc: 125 == GUI
	SDLK_UNKNOWN,
	// no keys: 126..127 == ?, MENU
	SDLK_UNKNOWN, SDLK_UNKNOWN,
};


/* Keyboard buffer; the values are SDL_Keycode values if > 0 and
 * releases when < 0.  Note that 0 is SDLK_UNKNOWN, and this is
 * used as an end marker in the buffer.
 *
 * For now, no history.  Possibly expand later, so that :-
 * The pointer for _wr points to the next position to write, and
 * _rd points to the next position to read.  The buffer loops.
 */
int32_t keybuf [4] = {
	SDLK_UNKNOWN, SDLK_UNKNOWN, SDLK_UNKNOWN, SDLK_UNKNOWN
};


/* Whether focus was entered or left since the last report of such
 * a change.  This can be useful to reset the keyboard lines shown
 * the emulated ZX Spectrum.  The flags are cleared when they are
 * reported.  When both are present, they cancel out.
 */
bool wlzx_keyboard_focus_enter = true;
bool wlzx_keyboard_focus_leave = false;


/* Enter or leave the period with keyboard focus.  These are reported
 * as SDL events of type SDL_WINDOWEVENT with event.window contents
 * and event.window.event set to SDL_WINDOWEVENT_ENTER or
 * SDL_WINDOWEVENT_LEAVE, respectively.  This is useful to stop the
 * input lines shown to the ZX Spectrum; without this, the key would
 * repeat indefinitely.
 */
void wlzx_keyboard_enter (void *data, struct wl_keyboard *self,
			uint32_t serial, struct wl_surface *surface, struct wl_array *keys) {
	fprintf (stderr, "DEBUG: wl_keyboard enter focus\n");
	if (wlzx_keyboard_focus_leave) {
		//
		// Enter followed by leave before any event to report
		wlzx_keyboard_focus_leave = false;
	} else {
		//
		// We have actually entered the keyboard focus period
		wlzx_keyboard_focus_enter = true;
	}
}
//
void wlzx_keyboard_leave (void *data, struct wl_keyboard *self,
			uint32_t serial, struct wl_surface *surface) {
	fprintf (stderr, "DEBUG: wl_keyboard leave focus\n");
	if (wlzx_keyboard_focus_enter) {
		//
		// Leave followed by enter does not need key line reset
		wlzx_keyboard_focus_enter = false;
	} else {
		//
		// We have actually left the keyboard focus period
		wlzx_keyboard_focus_leave = true;
	}
}


/* Receive a wl_keyboard's key event over the wl_keyboard_listener,
 * https://wayland.app/protocols/wayland#wl_keyboard:event:key
 *
 * A key was pressed or released. The time argument is a
 * timestamp with millisecond granularity, with an undefined base.
 * @param serial serial number of the key event
 * @param time timestamp with millisecond granularity
 * @param key key that produced the event
 * @param state physical state of the key
 *
 * Here, @param state is either WL_KEYBOARD_KEY_STATE_RELEASED
 * or WL_KEYBOARD_KEY_STATE_PRESSED.
 *
 * Keys are mapped to evento.type and evento.key.keysym.sym.
 * Since these fit in a 31-bit space, they can be negated for UP.
 *
 * For the SDL_Event structure, see
 * https://wiki.libsdl.org/SDL2/SDL_KeyboardEvent
 */
void wlzx_keyboard_key (void *data, struct wl_keyboard *self,
			uint32_t serial, uint32_t time, uint32_t key, uint32_t state) {
	//
	// Map the key scancode to a keysym value (in SDLK_xxx notation)
	int32_t keysym = (key * sizeof (*wlzx_keymap) < sizeof (wlzx_keymap))
					? wlzx_keymap [key]
					: SDLK_UNKNOWN;
	fprintf (stderr, "DEBUG: scancode = %d, keysym = %d = '%c' » ", key, keysym, ((keysym >= 32) && (keysym < 127)) ? (char) keysym : '?');
	//
	// Find the keybuf index to use
	unsigned kbi = 0;
	while (keybuf [kbi] != SDLK_UNKNOWN) {
		kbi++;
		if (kbi > sizeof (keybuf) / sizeof (*keybuf)) {
			fprintf (stderr, "Keyboard buffer overflow\n");
			return;
		}
	}
	//
	// Store the keysym value
	keybuf [kbi] = keysym;
	//
	// Flip if not _PRESSED
	if (state != WL_KEYBOARD_KEY_STATE_PRESSED) {
		keybuf [kbi] = -keybuf [kbi];
	}
	fprintf (stderr, "DEBUG: keybuf += %d\n", keybuf [kbi]);
}


/* Receive a wl_keyboard's modifier event over the wl_keyboard_listener,
 * https://wayland.app/protocols/wayland#wl_keyboard:event:modifiers
 *
 * Notifies clients that the modifier and/or group state has
 * changed, and it should update its local state.
 * @param serial serial number of the modifiers event
 * @param mods_depressed depressed modifiers
 * @param mods_latched latched modifiers
 * @param mods_locked locked modifiers
 * @param group keyboard layout
 *
 * This is not used for the ZX Spectrum, since scancodes arrive at key().
 */
void wlzx_keyboard_modifiers (void *data, struct wl_keyboard *self,
			uint32_t serial,
			uint32_t mods_depressed, uint32_t mods_latched, uint32_t mods_locked,
			uint32_t group) {
	//
	// Not used.  Modifiers are reported via key() as scancodes
	;
}


/* If event is not NULL, the next event is removed from the queue and stored
 * in the SDL_Event structure pointed to by event. The 1 returned refers to
 * this event, immediately stored in the SDL Event structure -- not an event
 * to follow.
 *
 * If event is NULL, it simply returns 1 if there is an event in the queue,
 * but will not remove it from the queue.
 *
 * As this function may implicitly call SDL_PumpEvents(), you can only call
 * this function in the thread that set the video mode.
 *
 * SDL_PollEvent() is the favored way of receiving system events since it
 * can be done from the main loop and does not suspend the main loop while
 * waiting on an event to be posted.
 *
 * Returns 1 if there is a pending event or 0 if there are none available.
 *
 * Delivery as SDL_KeyboardEvent via SDL as SDL_KEYUP or SDL_KEYDOWN,
 * no data in timestamp, windowID, state and, since repeat != 0 is
 * ignored, don't deliver repeats either.
 */
int SDL_PollEvent (SDL_Event *evento) {
	//
	// Process any pending elements from Wayland (without blocking)
	wlzx_dispatch (false);
	//
	// Look if we should report an event for enter/leave keyboard focus
	// Note: We will not poll something as spurious as focus change
	if ((evento != NULL) && (wlzx_keyboard_focus_enter || wlzx_keyboard_focus_leave)) {
		evento->type = SDL_WINDOWEVENT;
		if (wlzx_keyboard_focus_enter) {
			//
			// Handle the entry of the focus period
			evento->window.event = SDL_WINDOWEVENT_ENTER;
			wlzx_keyboard_focus_enter = false;
		} else {
			//
			// Handle the leave of the focus period
			evento->window.event = SDL_WINDOWEVENT_LEAVE;
			wlzx_keyboard_focus_leave = false;
		}
		//
		// Indicate the focus enter-or-leave event
		return 1;
	}
	//
	// Fail when no key event was found
	if (*keybuf ==  SDLK_UNKNOWN) {
		return 0;
	}
	//
	// If there is no evento, we are polling and should return now
	if (evento == NULL) {
		return 1;
	}
	//
	// Return the key that was found
	if (*keybuf < 0) {
		//
		// Return the key as having been released
		evento->type = SDL_KEYUP;
		evento->key.keysym.sym = - *keybuf;
		evento->key.repeat = 0;
	} else {
		//
		// Return the key as having been pressed
		evento->type = SDL_KEYDOWN;
		evento->key.keysym.sym = *keybuf;
		evento->key.repeat = 0;
	}
	//
	// Repeat neither a key press nor a key release (for non-modifiers)
	memcpy (keybuf, &keybuf [1], sizeof (keybuf) - sizeof (*keybuf));
	keybuf [sizeof (keybuf) / sizeof (*keybuf) - 1] = SDLK_UNKNOWN;
	//
	// Indicate that something was present
	return 1;
}


/* If event is not NULL, the next event is removed from the queue and stored
 * in the SDL_Event structure pointed to by event.
 *
 * Returns 1 on success or 0 if there was an error while waiting for events.
 */
int SDL_WaitEvent (SDL_Event *evento) {
	while (!SDL_PollEvent (evento)) {
		wlzx_dispatch (true);
	}
	return 1;
}


}   /* End of extern "C" */
